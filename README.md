
# docker-code-server

## Why

This project has been cloned from [marcadetd/docker-code-server](https://github.com/marcadetd/docker-code-server) which is itself a fork of [linuxserver/docker-code-server](https://github.com/linuxserver/docker-code-server).

The purpose of this is to be able to adapt this image to my needs and produce images in gitlab-research container registry while still being able to apply changes coming from the initial project.

When such changes happen:
- create a PR on [marcadetd/docker-code-server](https://github.com/marcadetd/docker-code-server)
- get the corresponding patch file by appending `.patch` to the PR URL (see [here](https://coderwall.com/p/6aw72a/creating-patch-from-github-pull-request))
- apply the PR on [marcadetd/docker-code-server](https://github.com/marcadetd/docker-code-server) and the corresponding patch to this repository

## Changes

- Use latest LTS of Ubuntu (currently `jammy` or `22.04`) so that it is easier to install latest versions of needed tools (like compilers)
- Install preffered and/or needed tools for images which will be based on this one
  - zsh
  - python3

## Details

- The exposed port is 8443
- The user folder is `/config`
- the user and sudo password is `abc`
- if docker is installed on your computer, you can run this image, assuming you are in a specific folder that 
  will be shared with the container at `/config`, with:
  `docker run -p 8443:8443 -v "$(pwd):/config"
    gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-code-server`

