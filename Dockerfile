FROM ghcr.io/linuxserver/baseimage-ubuntu:jammy

ARG CODE_RELEASE

# environment settings
ENV HOME="/config"

RUN \
  echo "**** install node repo ****" && \
  curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && \
  echo 'deb https://deb.nodesource.com/node_14.x jammy main' \
    > /etc/apt/sources.list.d/nodesource.list && \
  echo "**** install build dependencies ****" && \
  apt-get update && \
  apt-get install -y \
    build-essential \
    nodejs && \
  echo "**** install runtime dependencies ****" && \
  apt-get install -y \
    git \
    jq \
    libatomic1 \
    nano \
    net-tools \
    sudo && \
  echo "**** install code-server ****" && \
  if [ -z ${CODE_RELEASE+x} ]; then \
    CODE_RELEASE=$(curl -sX GET https://api.github.com/repos/coder/code-server/releases/latest \
    | awk '/tag_name/{print $4;exit}' FS='[""]' | sed 's|^v||'); \
  fi && \
  mkdir -p /app/code-server && \
  curl -o \
    /tmp/code-server.tar.gz -L \
    "https://github.com/coder/code-server/releases/download/v${CODE_RELEASE}/code-server-${CODE_RELEASE}-linux-amd64.tar.gz" && \
  tar xf /tmp/code-server.tar.gz -C \
    /app/code-server --strip-components=1 && \
  echo "**** install wanted tools ****" && \
  apt-get install -y \
    python3 \
    python3-pip \
    zsh && \
  echo "**** clean up ****" && \
  apt-get purge --auto-remove -y \
    build-essential \
    nodejs && \
  apt-get clean && \
  rm -rf \
    /config/* \
    /tmp/* \
    /var/lib/apt/lists/* \
    /var/tmp/* \
    /etc/apt/sources.list.d/nodesource.list

# add local files
COPY /root /

# set sudo password
ARG SUDO_PASSWORD=abc
RUN echo "abc ALL=(ALL:ALL) ALL" >> /etc/sudoers && \
    echo "${SUDO_PASSWORD}\n${SUDO_PASSWORD}" | passwd abc

# Install on-my-zsh
RUN chsh --shell /bin/zsh abc && \ 
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" && \
    git clone --branch master --single-branch --depth 1 \
        "https://github.com/zsh-users/zsh-autosuggestions.git" \
        /config/.oh-my-zsh/plugins/zsh-autosuggestions && \
    echo "source /config/.oh-my-zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh" \ 
        >> /config/.zshrc && \
    git clone --branch master --single-branch --depth 1 \
        "https://github.com/zsh-users/zsh-syntax-highlighting.git" \
        /config/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting && \
    sed -i 's/plugins=(.*/plugins=(git vscode)/' /config/.zshrc && \
    echo 'PS1="%B%F{blue}%~:%f%b "'           >>/config/.zshrc

# VSCode extension for Python
RUN /app/code-server/bin/code-server --extensions-dir /config/extensions --install-extension ms-python.python

# Default workspace
RUN mkdir /config/workspace

# /config will be a docker volume, initially empty
# wrapper_script will do the copy from /init-config
# but /config must exist fort the mount
RUN mv /config /init-config && \
    mkdir /config

# launch
ENTRYPOINT ["/bin/bash", "/usr/local/lib/wrapper_script.sh"]

# ports and volumes
EXPOSE 8443
