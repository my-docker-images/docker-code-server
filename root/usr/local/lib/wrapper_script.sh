#!/bin/bash

if [ -z $2 ]; then
    AUTH="none"
else
    AUTH="password"
    export PASSWORD=$2
fi

cp -Rn /init-config/*         /config/
cp -Rn /init-config/\.[a-z]*  /config/
chown -R abc:abc /config

# Environment
export TZ=Europe/Paris
export SUDO_PASSWORD=abc

exec \
    s6-setuidgid abc \
        /app/code-server/bin/code-server \
            --bind-addr 0.0.0.0:8443 \
            --user-data-dir /config/data \
            --extensions-dir /config/extensions \
            --disable-telemetry \
            --auth "${AUTH}" \
            /config/workspace
